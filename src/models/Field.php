<?php

namespace app\models;


class Field
{
    protected $cells;
    protected $coins;
    protected $permutations = [];

    public function __construct($cells, $coins)
    {
        $this->cells = range(1, $cells);
        $this->coins = $coins;
    }

    public function getPossiblePermutationsNumber()
    {
        $this->permutate(0, [], 0);

        return count($this->permutations);
    }

    /**
     * @param $dataIndex int индекс во временном массиве комбинаций
     * @param $data array сам массив комбинаций
     * @param $cellsIndex int индекс в массиве клеток
     *
     * суть в том, что текущая клетка может быть включена в комбинацию, а может быть и нет
     * мы идём по клеткам и рекурсивно уходим в оба случая
     */
    protected function permutate($dataIndex, $data, $cellsIndex)
    {
        // составили комбинацию
        if ($dataIndex == $this->coins) {
            $permutation = [];
            for ( $j = 0; $j < $this->coins; $j++) {
                $permutation[] = $data[$j];
            }
            $this->permutations[] = $permutation;
            return;
        }

        if ($cellsIndex >= count($this->cells)) {
            return;
        }

        // ставим монету сюда, а следующую дальше
        $data[$dataIndex] = $this->cells[$cellsIndex];
        $this->permutate($dataIndex + 1, $data, $cellsIndex + 1);

        // вариант, когда не ставим монету, а меняем её на следующую
        $this->permutate($dataIndex, $data, $cellsIndex + 1);
    }

    public function permutationsToString()
    {
        if (count($this->permutations) < 10) {
            return 'Менее 10 вариантов';
        }
        $string = count($this->permutations) . PHP_EOL;

        foreach ($this->permutations as $p) {
            $string .= '(' . implode(',', $p) . ')' . PHP_EOL;
        }

        return $string;
    }
}