<?php

namespace app\components;

class Controller
{
    public function render($view, $vars)
    {
        $templatePath = APPLICATION_PATH . '/views/';

        $file = $templatePath . $view  . '.php';

        if (file_exists($file)) {
            if (is_array($vars) && !empty($vars)) {
                extract($vars);
            }
            ob_start();
            include $file;
            return ob_get_clean();
        } else {
            throw new \Exception('Template ' . $file . ' not found!');
        }
    }
}