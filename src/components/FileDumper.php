<?php

namespace app\components;


class FileDumper
{
    public function dump($data, $filename)
    {
        $path = PUBLIC_PATH . '/files/';

        if (false === file_put_contents($path . $filename, $data)) {
            throw new \Exception('Cannot save the file ' . $path . $filename);
        }
    }
}