<?php

namespace app\controllers;

use app\components\FileDumper;
use app\models\Field;
use app\components\Controller;

class MainController extends Controller
{
    public function index()
    {
        if ($_REQUEST['cells'] && $_REQUEST['coins']) {
            $field = new Field($_REQUEST['cells'], $_REQUEST['coins']);
            $permutations = $field->getPossiblePermutationsNumber();
            $dumper = new FileDumper();
            $dumper->dump($field->permutationsToString(), 'permutations.txt');
        }

        return $this->render('index',['permutations' => $permutations, 'filename' => 'files/permutations.txt']);
    }
}