<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test</title>
</head>
<body>
<form action="/">
    <label for="cells">Клеток</label>
    <input type="text" name="cells" id="cells"><br>
    <label for="coins">Монеток</label>
    <input type="text" name="coins" id="coins"><br>
    <button type="submit">посчитать</button>
</form>
<div>
    <?php if ($permutations): ?>
        количество возможных перестановок: <?= $permutations ?><br>
        <a href="<?= $filename ?>">скачать файл с перестановками</a>
    <?php endif; ?>
</div>
</body>
</html>