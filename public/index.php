<?php

require __DIR__ . '/../vendor/autoload.php';

use app\controllers\MainController;

define('APPLICATION_PATH', dirname(dirname(__FILE__)) . '/src');
define('PUBLIC_PATH', dirname(dirname(__FILE__)) . '/public');
define('ROOT_PATH', dirname(dirname(__FILE__)));

// sry 'bout dat)
ini_set('max_execution_time', 300);

try {

    echo (new MainController())->index();

} catch (Exception $e) {
    echo 'Error: ', $e->getMessage();
    echo '<pre>', print_r($e->getTrace(), true), '</pre>';
}